const faker = require('faker');
const logger = require('pino')();

const Blockchain = require('./src/Blockchain');
const Block = require('./src/Block');

const blockchain = new Blockchain();

logger.info('Block mining started ...');

logger.info({ msg: 'Block 1 - start', hr_mn_s: new Date().toTimeString() });
blockchain.addNewBlock(
  new Block(1, Date.now(), {
    sender: faker.name.findName(),
    recipient: faker.name.findName(),
    quantity: faker.commerce.price(),
  }),
);
logger.info({ msg: 'Block 1 - done', hr_mn_s: new Date().toTimeString() });

logger.info({ msg: 'Block 2 - start', hr_mn_s: new Date().toTimeString() });
blockchain.addNewBlock(
  new Block(2, Date.now(), {
    sender: faker.name.findName(),
    recipient: faker.name.findName(),
    quantity: faker.commerce.price(),
  }),
);
logger.info({ msg: 'Block 2 - done', hr_mn_s: new Date().toTimeString() });

logger.info(blockchain);
