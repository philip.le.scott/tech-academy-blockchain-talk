# Description and Info
This repository contains the very basic makings of a blockchain, using ES6 and basic Object Oriented patterns to demonstrate
a technical basis of design for educational purposes. This is in no way a complete and functional distributed blockchain implementation
and should not be considered or expected to behave as such.

# Getting Started
- run `npm i`
- run `npm start`
